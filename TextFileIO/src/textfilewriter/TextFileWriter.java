package textfilewriter;

//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TextFileWriter extends Util {

    //File for storing where the text file is located
    //And for knowing which text file to use
    private File f;
    private FileWriter fw;
    private BufferedWriter bw;
    

    private ArrayList<String> stuffs = new ArrayList();

    /**
     * Nice way of handling writing to a text file
     *
     * @param filePath the path or name if the file is in the project directory
     * of the text file you want to write to
     *
     */
    public TextFileWriter(String filePath) {

        //try catch to avoid crashing the program if the user inputs an invalid file
        try {
            this.f = new File(filePath);
            this.f.setWritable(true);
            this.fw = new FileWriter(this.f, true);
            this.bw = new BufferedWriter(this.fw);

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Creates a list of string to send to the text file all at once
     *
     * @param s the string to add to the list
     */
    public void add(String s) {

        this.stuffs.add(s);
    }

    /**
     * Send the list of strings to the specified text file
     *
     * @throws IOException throws a IOException
     */
    public void output() throws IOException {

        if (!this.stuffs.isEmpty()) {
            for (int i = 0; i < this.stuffs.size(); i++) {
                this.bw.newLine();
                this.bw.write(this.stuffs.get(i));
            }
            this.bw.close();

        } else {
            System.err.println("No strings in the list");
            this.bw.close();
 
        }
    }

}
