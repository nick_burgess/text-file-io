package textfilewriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class TextFileReader {

    private File f;
    private BufferedReader br;
    private FileReader fr;
    private Scanner sc;
    private ArrayList<String> list = new ArrayList();

    private int pos = 0;

    /**
     * Create a new object to read from the specified text file
     *
     * @param filePath the file path or name of the text file to read from
     */
    public TextFileReader(String filePath) {
        try {
            this.f = new File(filePath);
            this.fr = new FileReader(this.f);
            this.br = new BufferedReader(this.fr);
            this.f.setReadable(true);
            this.sc = new Scanner(this.f);

        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Add all of the lines in the text file to the array of lines
     */
    public void update() {

        this.list.removeAll(Collections.EMPTY_LIST);

        while (this.sc.hasNextLine()) {
            this.list.add(this.sc.nextLine());
            this.pos++;
        }
    }

    private void getAtPos() {
        this.list.clear();
        update();
        if (pos > this.list.size()) {
            pos = 0;
        }
    }

    /**
     * Reads the next line in the text file if it will not run off the end of
     * the file
     *
     * @return the next line if it will not run off the end of the file
     */
    public String getNextLine() {

        update();
        getAtPos();
        if (!list.isEmpty()) {
            return this.list.get(pos);
        } else {
            return null;
        }
    }

    /**
     * Reads the specified line of the text file
     *
     * @param line the line to read starting at 0
     * @return the line if the specified line exist
     * @return null if the line does not exist
     */
    public String getLine(int line) {

        update();
        if (!this.list.isEmpty() && line < this.list.size()) {
            return String.valueOf(this.list.get(line));
        } else {
            //throw new IllegalArgumentException("The specified line must be a reachable line in the text file");
            return null;
        }
    }

}
