package textfilewriter;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Util {

    //Calendar for get the date and time information
    private Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Canada/Eastern"));

    //Variables for date information such as month, day of the month and year
    private int month = this.calendar.get(Calendar.MONTH);
    private int day = this.calendar.get(Calendar.DAY_OF_MONTH);
    private int year = this.calendar.get(Calendar.YEAR);

    //Variables for holding time information such as hour, minute, second and milliseconds 
    //It uses the values of time when the program started as a placeholder
    private int hour = this.calendar.get(Calendar.HOUR_OF_DAY);
    private int minute = this.calendar.get(Calendar.MINUTE);
    private int second = this.calendar.get(Calendar.SECOND);
    private int millisecond = this.calendar.get(Calendar.MILLISECOND);

    //A string for the date stamp containing the day, month and year
    private String dateStamp = this.day + "/" + this.month + "/" + this.year;

    private String timeStamp = this.hour + ":" + this.minute + ":" + this.second + "." + this.millisecond;

    /**
     * Refresh the time values for the time stamp
     */
    public void updateTime() {
        this.hour = this.calendar.get(Calendar.HOUR_OF_DAY);
        this.minute = this.calendar.get(Calendar.MINUTE);
        this.second = this.calendar.get(Calendar.SECOND);
        this.millisecond = this.calendar.get(Calendar.MILLISECOND);

        this.timeStamp = this.hour + ":" + this.minute + ":" + this.second + "." + this.millisecond;
    }

    /**
     * Refresh the date values for the date stamps
     */
    public void updateDate() {
        this.day = this.calendar.get(Calendar.DAY_OF_MONTH);
        this.month = this.calendar.get(Calendar.MONTH);
        this.year = this.calendar.get(Calendar.YEAR);

        this.dateStamp = this.day + "/" + this.month + "/" + this.year;
    }

    /**
     * Gets the current date stamp
     *
     * @return the current date stamp
     */
    public String getDateStamp() {
        updateDate();
        return this.dateStamp;
    }

    /**
     * Gets the current time stamp
     *
     * @return the current time stamp
     */
    public String getTimeStamp() {
        updateTime();
        return this.timeStamp;
    }

    /**
     * Refreshes the current time and date values
     */
    public void update() {
        updateDate();
        updateTime();
    }

}
